#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>


int main() {
    const char *dev_path = "/dev/input/event0";
    int fd = open(dev_path, O_RDONLY);
    if (fd < 0) {
        printf("Could not open %s; make sure the device exists and you're executing with root.", dev_path);
        return 1;
    }

    struct input_event event;

    for (;;) {
        if (read(fd, &event, sizeof(event)) < 0)
            break;
        if (event.value == 0) {
            // pressed
            printf("Pressed key %d", event.code);
        } else if (event.value == 1) {
            // released 
            printf("Released the key\n");
        }
    }

    close(fd);   

    return 0;
}