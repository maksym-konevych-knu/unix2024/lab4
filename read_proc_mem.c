#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <unistd.h>


int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <pid> <address>\n", argv[0]);
        return 1;
    }
    const char *s_pid = argv[1];
    const char *s_address = argv[2];

    unsigned pid = (unsigned)atoi(s_pid);
    unsigned address = (unsigned)atoi(s_address);

    printf("Starting to monitor pid %u at %u\n", pid, address);

    char file[64];
    sprintf(file, "/proc/%ld/mem", (long)pid);
    int fd = open(file, O_RDWR);
    if (fd < 0) {
        printf("Could not open...\n");
        return 1;
    } else {
        printf("Opened %s sucessfully.\n", file);
    }

    for (;;) {
        // ptrace(PTRACE_ATTACH, pid, 0, 0);
        // waitpid(pid, NULL, 0);

        off_t addr = address; // target process address
        char a[11] = {};
        if (pread(fd, a, sizeof(a) - 1, addr) < 0) {
            printf("Could not read...\n");
            break;
        }
        printf("Read line: %s\n", a);

        // ptrace(PTRACE_DETACH, pid, 0, 0);
        // waitpid(pid, NULL, 0);

        sleep(1);
    }

    close(fd);

    return 0;
}