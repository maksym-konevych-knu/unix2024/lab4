#include <stdio.h>

int main() {
    char a;
    printf("Adress: %u\n", &a);
    for (;;) {
        char new_a = getc(stdin);
        if (new_a != '\n') {
            a = new_a;
            printf("Updated!\n");
        }
    }
    return 0;
}